<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Promocao extends Model
{
    public $timestamps = false;

    public $table = "promocoes";

    public $fillable = ['nome','banner','oferta','valor','descricao'];


    public function getPromocoes()
    {
        return self::all();
    }

    public function savePromocao()
    {
        $request = Input::all();

        $promocao = new Promocao();
        if(!empty($request['anunciante_id']))
            $promocao->anunciante_id = $request['anunciante_id'];
        if(!empty($request['categoria_id']))
            $promocao->categoria_id = $request['categoria_id'];
        if(!empty($request['nome']))
            $promocao->nome = $request['nome'];
        if(!empty($request['banner']))
            $promocao->banner = $request['banner'];
        if(!empty($request['oferta']))
            $promocao->oferta = $request['oferta'];
        if(!empty($request['valor']))
            $promocao->valor = $request['valor'];
        if(!empty($request['limitada']))
            $promocao->limitada = $request['limitada'];
        if(!empty($request['descricao']))
            $promocao->descricao = $request['descricao'];
        if(!empty($request['abertura']))
            $promocao->abertura = $request['abertura'];
        if(!empty($request['validade']))
            $promocao->validade = $request['validade'];

            $promocao->criacao = date('Y-m-d H:i:s');
            $promocao->atualizacao = date('Y-m-d H:i:s');
            $promocao->save();

        return $promocao;
    }

    public function getPromocao($id)
    {
        $promocao = self::find($id);

        if(is_null($promocao))
        {
           return false;
        }
        return $promocao;
    }

    public function deletePromocao($id)
    {
        $promocao = self::find($id);

        if(is_null($promocao))
        {
            return false;
        }
        return $promocao->delete();
    }

    public function updatePromocao($id)
    {
        $promocao = self::find($id);

        if(is_null($promocao))
        {
            return false;
        }
        $request = Input::all();
            if(!empty($request['anunciante_id']))
                $promocao->anunciante_id = $request['anunciante_id'];
            if(!empty($request['categoria_id']))
                $promocao->categoria_id = $request['categoria_id'];
            if(!empty($request['nome']))
                $promocao->nome = $request['nome'];
            if(!empty($request['banner']))
                $promocao->banner = $request['banner'];
            if(!empty($request['oferta']))
                $promocao->oferta = $request['oferta'];
            if(!empty($request['valor']))
                $promocao->valor = $request['valor'];
            if(!empty($request['limitada']))
                $promocao->limitada = $request['limitada'];
            if(!empty($request['descricao']))
                $promocao->descricao = $request['descricao'];
            if(!empty($request['abertura']))
                $promocao->abertura = $request['abertura'];
            if(!empty($request['validade']))
                $promocao->validade = $request['validade'];

                $promocao->atualizacao = date('Y-m-d H:i:s');
                $promocao->save();

        return $promocao;
    }
}
