<?php

namespace App\Http\Controllers;

use App\Anunciante;
use App\Usuario;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class AnunciantesController extends Controller
{
    function __construct()
    {

    }

    public function index()
    {

    }

    public function create()
    {
        return view("anunciantes.create");
    }

    public function createUser(Request $request)
    {
        $novoUsuario = new Usuario();

        $novoUsuario->email = $request->nome;
        $novoUsuario->senha = $request->senha;
        $novoUsuario->save();

        return $novoUsuario;
    }

    public function store(Request $request)
    {
        $usuario = $this->createUser($request);

        $anunciante = new Anunciante();

        if(empty($request->ponto_comercial))
            $anunciante->nome = $request->ponto_comercial;

        if(empty($request->ponto_cnpj))
            $anunciante->banner = $request->cnpj;

        if(empty($request->nome_fantasia))
            $anunciante->descricao = $request->nome_fantasia;

        if(empty($request->logo))
            $anunciante->descricao = $request->logo;

        $anunciante->id_usuario = $usuario->id_usuario;
        $anunciante->criacao = date ( 'Y-m-d H:i:s' );
        $anunciante->atualizacao = date ( 'Y-m-d H:i:s' );

        $anunciante->save();

        return redirect()->action('PromocoesController@index');
    }

    public function edit($id)
    {
        $promocao = Promocao::where('id','=',$id)->first();

        return view("edit")->with("promocao",$promocao);
    }

    public function update($id , Request $request)
    {
        $promocao = Promocao::where('id','=',$id)->first();

        $promocao->nome = $request->nome;
        $promocao->banner = $request->banner;
        $promocao->descricao = $request->descricao;
        $promocao->atualizacao = date ( 'Y-m-d H:i:s' );

        $promocao->save();

        return redirect()->action('PromocoesController@index');

    }

    public function delete($id)
    {
        $promocao = Promocao::find($id);
        $promocao->delete();

        return redirect()->action('PromocoesController@index');
    }
}
