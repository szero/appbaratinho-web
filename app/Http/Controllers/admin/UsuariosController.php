<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

use App\Http\Requests;

class UsuariosController extends Controller
{
    function __construct()
    {

    }

    public function index()
    {
        return View::make();
    }

    public function create()
    {
        return View::make();
    }

    public function store(Request $request)
    {
        return View::make();
    }

    public function edit()
    {
        return View::make();
    }

    public function update(Request $resquest)
    {
        return View::make();
    }

    public function delete($id)
    {
        return View::make();
    }
}
