<?php

namespace App\Http\Controllers;

use App\Promocao;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\View\View;

class PromocoesController extends Controller
{
    function __construct()
    {

    }

    public function index()
    {
        $promocoes = Promocao::all();

        return view("show")->with("promocoes",$promocoes);
    }

    public function create()
    {
        return view("create");
    }

    public function store(Request $request)
    {
        $promocao = new Promocao();

        $promocao->nome = $request->nome;
        $promocao->banner = $request->banner;
        $promocao->descricao = $request->descricao;
        $promocao->criacao = date ( 'Y-m-d H:i:s' );
        $promocao->atualizacao = date ( 'Y-m-d H:i:s' );

        $promocao->save();


        return redirect()->action('PromocoesController@index');
    }

    public function edit($id)
    {
        $promocao = Promocao::where('id','=',$id)->first();

        return view("edit")->with("promocao",$promocao);
    }

    public function update($id , Request $request)
    {
        $promocao = Promocao::where('id','=',$id)->first();

        $promocao->nome = $request->nome;
        $promocao->banner = $request->banner;
        $promocao->descricao = $request->descricao;
        $promocao->atualizacao = date ( 'Y-m-d H:i:s' );

        $promocao->save();

        return redirect()->action('PromocoesController@index');

    }

    public function delete($id)
    {
        $promocao = Promocao::find($id);
        $promocao->delete();

        return redirect()->action('PromocoesController@index');
    }
}
