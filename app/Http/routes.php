<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/','IndexController@index');
Route::get('admin','admin\PainelController@index');
Route::get('admin/graficos','admin\PainelController@graficos');
Route::get('admin/forms','admin\PainelController@forms');
Route::get('admin/tabelas','admin\PainelController@tabelas');


//////////////////ROTAS ///////////////////////////////


//rotas para as promocoes
Route::get('promocoes','PromocoesController@index');
Route::get('promocoes/edit/{id}','PromocoesController@edit');
Route::get('promocoes/create','PromocoesController@create');
Route::post('promocoes/store','PromocoesController@store');
Route::post('promocoes/update/{id}','PromocoesController@update');
Route::get('promocoes/delete/{id}','PromocoesController@delete');


//rotas para as Usuarios
Route::get('usuarios','UsuariosController@index');
Route::get('usuarios/edit/{id}','UsuariosController@edit');
Route::get('usuarios/create','UsuariosController@create');
Route::post('usuarios/store','UsuariosController@store');
Route::post('usuarios/update/{id}','UsuariosController@update');
Route::post('usuarios/delete/{id}','UsuariosController@delete');

//rotas para as Anunciantes
Route::get('anunciantes','AnunciantesController@index');
Route::get('anunciantes/edit/{id}','AnunciantesController@edit');
Route::get('anunciantes/create','AnunciantesController@create');
Route::post('anunciantes/store','AnunciantesController@store');
Route::post('anunciantes/update/{id}','AnunciantesController@update');
Route::post('anunciantes/delete/{id}','AnunciantesController@delete');


//////////ROTAS PARA O ADMIN ///////////
Route::group(['middleware' => ['auth','menu']], function() {

    //rotas para as promocoes
    Route::get('admin/promocoes','admin\PromocoesController@index');
    Route::get('admin/promocoes/edit/{id}','admin\PromocoesController@edit');
    Route::get('admin/promocoes/create','admin\PromocoesController@create');
    Route::post('admin/promocoes/store','admin\PromocoesController@store');
    Route::post('admin/promocoes/update/{id}','admin\PromocoesController@update');
    Route::post('admin/promocoes/delete/{id}','admin\PromocoesController@delete');


    //rotas para as Usuarios
    Route::get('admin/usuarios','admin\UsuariosController@index');
    Route::get('admin/usuarios/edit/{id}','admin\UsuariosController@edit');
    Route::get('admin/usuarios/create','admin\UsuariosController@create');
    Route::post('admin/usuarios/store','admin\UsuariosController@store');
    Route::post('admin/usuarios/update/{id}','admin\UsuariosController@update');
    Route::post('admin/usuarios/delete/{id}','admin\UsuariosController@delete');

    //rotas para as Anunciantes
    Route::get('admin/anunciantes','admin\AnunciantesController@index');
    Route::get('admin/anunciantes/edit/{id}','admin\AnunciantesController@edit');
    Route::get('admin/anunciantes/create','admin\AnunciantesController@create');
    Route::post('admin/anunciantes/store','admin\AnunciantesController@store');
    Route::post('admin/anunciantes/update/{id}','admin\AnunciantesController@update');
    Route::post('admin/anunciantes/delete/{id}','admin\AnunciantesController@delete');

});


//////////ROTAS PARA API ///////////
//Route::group(['middleware' => ['auth','menu']], function() {

    //rotas para as promocoes
    Route::get('api/promocoes','api\PromocoesController@index');
    Route::get('api/promocoes/edit/{id}','api\PromocoesController@edit');
    Route::get('api/promocoes/create','api\PromocoesController@create');
    Route::post('api/promocoes/store','api\PromocoesController@store');
    Route::put('api/promocoes/update/{id}','api\PromocoesController@update');
    Route::delete('api/promocoes/delete/{id}','api\PromocoesController@delete');


    //rotas para as Usuarios
    Route::get('admin/usuarios','admin\UsuariosController@index');
    Route::get('admin/usuarios/edit/{id}','admin\UsuariosController@edit');
    Route::get('admin/usuarios/create','admin\UsuariosController@create');
    Route::post('admin/usuarios/store','admin\UsuariosController@store');
    Route::post('admin/usuarios/update/{id}','admin\UsuariosController@update');
    Route::post('admin/usuarios/delete/{id}','admin\UsuariosController@delete');

    //rotas para as Anunciantes
    Route::get('admin/anunciantes','admin\AnunciantesController@index');
    Route::get('admin/anunciantes/edit/{id}','admin\AnunciantesController@edit');
    Route::get('admin/anunciantes/create','admin\AnunciantesController@create');
    Route::post('admin/anunciantes/store','admin\AnunciantesController@store');
    Route::post('admin/anunciantes/update/{id}','admin\AnunciantesController@update');
    Route::post('admin/anunciantes/delete/{id}','admin\AnunciantesController@delete');

//});


///////////////////////ROTAS DE AUTENTICACAO ////////////////////

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');


Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
