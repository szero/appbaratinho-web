<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/style.css"/>
    <title>Title</title>
</head>
<body>
<div class="bg-top">
    <div class="container">
        <div class="container-conteudo">
            <header class="cabecalho">
                <img class="logo-header" src="/_images/Logo-baratinho.png"/>
                <nav class="menu">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">O Projeto</a></li>
                        <li><a href="#">Dúvidas Frequentes</a></li>
                        <li><a href="#">Contato</a></li>
                    </ul>
                    <div  class="btn-top">
                        <a href="#" class="btn-text">Anuncie Grátis</a>
                    </div>
                </nav>
            </header>
            <div class="banner-topo">
                <div class="banner-frase-1-bg">
                    <h2 class="banner-frase-1">
                        Anuncie grátis para mais de
                    </h2>
                </div>
                <div class="banner-baixo">
                    <div class="banner-frase-2-bg">
                        <div class="obj-banner"></div>
                        <h2 class="banner-frase-2">
                            100.000 Clientes!
                        </h2>
                    </div>
                </div>
            </div>
            <div class="frase-destaque">
                Encontre a <span style="color: #2f9442; font-family: "FontePadraoBold", sans-serif">promoção</span> mais próxima de <span style="color: #2f9442; font-family:"FontePadraoBold", sans-serif">VOCÊ!</span>
            </div>
            <div class="conteudo">
                <div class="lado-esquerdo">
                    <h2 class="h2-padrao">Escolha Categorias</h2>
                    <form class="form-esquerdo">
                        <input type="checkbox" name="categoria-1" value="churrascaria" checked><span class="item-form">Churrascarias</span> <br>
                        <input type="checkbox" name="categoria-2" value="restaurante" checked class="item-form-margin"><span class="item-form">Restaurante</span><br>
                        <input type="checkbox" name="categoria-3" value="salao-de-beleza" checked class="item-form-margin"><span class="item-form">Salão de Beleza</span><br>
                        <input type="checkbox" name="categoria-4" value="fast-food" checked class="item-form-margin"><span class="item-form">Fast Food</span><br>
                        <input type="checkbox" name="categoria-5" value="academia" checked class="item-form-margin"><span class="item-form">Academias</span><br>
                    </form>
                    <hr class="linha">
                    <h2 class="h2-padrao">Cidade</h2>
                    <h3 class="h3-esquerdo">Porto Alegre - Rio Grande do Sul</h3>
                    <input type="text" class="textbox" placeholder="Digite e tecle ENTER"/>
                    <hr class="linha">
                    <div class="lado-direito">
                        <img src="_images/mapa.png"/>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="footer-conteudo"></div>
                <div class="footer-esquerdo">
                    <img src="_images/logo-baratinho-footer.png"/>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec .Sed dui lorem, adipiscing in adipiscing et, interdum nec .Sed dui lorem, adipiscing in adipiscing et, interdum nec .Sed dui lorem, adipiscing in adipiscing et, interdum nec . Lorem ipsum dolor sit amet, consectetur </p>
                </div>
                <div class="footer-direito">
                </div>
                <div class="footer-centro">
                    <div class="footer-centro-conteudo">
                        <h2>Navegação Rápida</h2>
                        <a href="#">O Projeto</a><br>
                        <a href="#">FAQ</a><br>
                        <a href="#">Contato</a><br>
                        <a href="#">Como Anunciar</a><br>
                        <a href="#">Tornar-se Parceiro</a><br>
                        <a href="#">Autores</a>
                    </div>
                </div>
                <div class="footer-baixo">
                    <p>Todos direitos reservados. Baratinho 2016.</p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>